require 'spec_helper'

describe 'DOHelpers' do
  let (:image) {"123"}
  let (:happy_res) {double(status:"OK",event_id:"123")}
  describe '#start server' do
    before do
      expect(Digitalocean::Droplet).to receive(:create).with( name:"ftb-monster", image_id: image, region_id:6, size_id:64).and_return(happy_res)
    end
    it 'should be able to start with the right params' do
      DOHelpers.start(image)
    end

    it 'should return event_id' do
      expect(DOHelpers.start(image)[:success]).to eq true
    end
  end


  describe '#update event' do
    let (:event_id) {123}
    let (:update_message) { double(status:"OK", event: {action_status:"DONE",percentage:"100"}) }

    before do
      expect(Digitalocean::Event).to receive(:find).with(event_id).and_return(update_message)
    end

    it 'should return event status' do
      expect(DOHelpers.update(123)).to eq({status:"DONE", percentage:"100"})
    end

  end
end

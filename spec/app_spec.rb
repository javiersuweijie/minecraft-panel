require 'spec_helper.rb'

describe 'App', :vcr do
  let(:response) {double}
  it 'should get home page' do
    get '/'
    expect(last_response).to be_successful
  end
end

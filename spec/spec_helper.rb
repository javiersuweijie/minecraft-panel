require File.expand_path '../../app.rb', __FILE__
require 'rack/test'
require 'vcr'

ENV['RACK_ENV'] = 'test'

VCR.configure do |c|
  c.cassette_library_dir = 'spec/cassettes'
  c.hook_into :webmock
  c.configure_rspec_metadata!
  c.default_cassette_options = { :record => :new_episodes }
end

module RSpecMixin
    include Rack::Test::Methods
      def app() Sinatra::Application end
end

# For RSpec 2.x
RSpec.configure do |c| 
  c.include RSpecMixin
  c.color = true
  c.formatter = :documentation
end

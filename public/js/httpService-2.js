angular.module('mcPanel')
.service("httpService", function($http) {
  this.start = function(image_id) {
    console.log(image_id)
    return $http.post("./droplet",{'image_id':image_id})
  }

  this.getUpdate = function(event_id) {
    return $http.get("./update",{params:{event_id:event_id}})
  }

  this.checkStatus = function() {
    return $http.get("./status")
  }

  this.getFTBImage = function() {
    return $http.get("./ftbimage")
  }

  this.destroy = function(droplet_id) {
    return $http.delete("./droplet",{'params':{'droplet_id':droplet_id}})
  } 
});

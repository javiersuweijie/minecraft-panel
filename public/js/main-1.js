app = angular.module("mcPanel",[])
app.controller("doController", function($scope,httpService) {
  $scope.online = false;
  $scope.image = {};
  $scope.imageLoading = true;
  $scope.droplet = {};

  $scope.check = function() {
    httpService.checkStatus().success(function(data) {
      if (data.data !== null) {
        $scope.droplet = data.data
        $scope.online  = true
      }
      console.log($scope.droplet)
    });
  };
   
  httpService.getFTBImage().success(function(data) {
    $scope.image = data.data;
    $scope.imageLoading = false;
  });

  $scope.startServer = function(id) {
    httpService.start(id).success(function(data) {
      $scope.check();
    });
  };

  $scope.destroyServer = function(id) {
    httpService.destroy(id).success(function(data) {
      $scope.destroying = true;
    });
  };
  $scope.check();
  
});

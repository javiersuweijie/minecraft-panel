require 'sucker_punch'
require 'sinatra'
require 'digitalocean'
require './do_helpers.rb'
require 'sinatra/json'

SuckerPunch.logger = Logger.new('sucker_punch.log')

Digitalocean.client_id = "45a6337937e98ca71458c31fca853a2b"
Digitalocean.api_key = "60984062850a1a426f7ac9aa6df87e81"

set :views, Proc.new { File.join(root, "public") }

get '/' do
  @js_files = `ls public/js/*.js`.split("\n").map {|each| each[6..-1]}.sort {|a,b| a[-4]<=>b[-4]}
  erb :index
end

post "/droplet" do
  data = JSON.parse(request.body.read)
  StartUpJob.new.async.perform(data['image_id'])
  json data: {success: true}
end

delete "/droplet" do
  ShutDownJob.new.async.perform(params['droplet_id'])
  json data: {success: true}
end

get "/update" do
  json data: DOHelpers.update(params[:event_id])
end

get "/status" do
  json data: DOHelpers.check_status
end

get "/ftbimage" do
  json data: DOHelpers.get_ftb_image
end

class DOHelpers
  SG_ID = 6
  SMALL_ID = 64
  def self.start(image_id)
    response = Digitalocean::Droplet.create(name: "ftb-monster", image_id: image_id, region_id: SG_ID, size_id:SMALL_ID)
    if response.status.match "OK"
      p response
      return {success:true, event: response.event_id}
    else
      return {success:false}
    end
  end

  def self.update(event_id)
    response = Digitalocean::Event.find(event_id)
    if response.status == "OK"
      return {status: response.event[:action_status], percentage: response.event[:percentage]}
    else
      return {status: "ERROR"}
    end
  end

  def self.check_status
    response = Digitalocean::Droplet.all
    if response.droplets.empty?
      nil
    else
      ftb_droplet = response.droplets.keep_if {|droplet| droplet.name == "ftb-monster"}.first
      {name: ftb_droplet[:name],id: ftb_droplet[:id], ip_address: ftb_droplet[:ip_address]}
    end
  end

  def self.get_ftb_image
    response = Digitalocean::Image.all
    if response.images.empty?
      nil
    else
      ftb_image = response.images.keep_if {|image| not image.name.match("ftb-monster").nil?}.first
      if ftb_image.nil?
        nil
      else
        {name: ftb_image[:name],id:ftb_image[:id]}
      end
    end
  end

  def self.power_off(droplet_id)
    response = Digitalocean::Droplet.power_off(droplet_id)
    if response.status.match "OK"
      return {success:true, event: response.event_id}
    else
      return {success:false}
    end
  end

  def self.snapshot(droplet_id)
    response = Digitalocean::Droplet.snapshot(droplet_id, name:"ftb-monster_#{Time.now.to_i}")
    if response.status.match "OK"
      return {success:true, event: response.event_id}
    else
      return {success:false}
    end
  end

  def self.destroy(droplet_id)
    response = Digitalocean::Droplet.destroy(droplet_id)
    if response.status.match "OK"
      return {success:true, event: response.event_id}
    else
      return {success:false}
    end
  end

  def self.clean_images
    response = Digitalocean::Image.all filter: "my_images"
    if response.status.match "OK"
      ftb_images = response.images.keep_if {|image| not image.name.match("ftb-monster").nil?}
      to_delete = ftb_images.map {|image| {id:image.id, time:image.name.split("_").last}}.sort {|a,b| a[:time]<=>b[:time]}[0...-1]
      to_delete.each {|each| Digitalocean::Image.destroy each[:id]}
    end
  end
end

class StartUpJob
  include SuckerPunch::Job
  def perform(image_id)
    DOHelpers.start(image_id)
  end
end

class ShutDownJob
  include SuckerPunch::Job
  def perform(droplet_id)
    success = false
    while not success
      p "Powering off"
      success = DOHelpers.power_off droplet_id
      sleep 3
    end
    success = false
    while not success
      p "Taking snapshot"
      success = DOHelpers.snapshot(droplet_id)[:success]
      sleep 3
    end
    success = false
    while not success
      p "Destorying"
      success = DOHelpers.destroy(droplet_id)[:success]
      sleep 6
    end
    DOHelpers.clean_images
  end
end
